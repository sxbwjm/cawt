package CLIGraphics;

import static org.fusesource.jansi.Ansi.ansi;
import static org.fusesource.jansi.Ansi.Color.RED;

import java.io.IOException;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;

import Common.Common;

public class CRectangle implements Shape
{
    public int x = 0, y = 0;
    public int width = 10, height = 5;

    public CRectangle()
    {

    }
    

    public CRectangle(int x, int y, int w, int h)
    {
        this.x = x; this.y = y; 
        width = w; height = h;
    }

   
    @Override
    public void draw(Graphics2D g)
    {
        AnsiConsole.systemInstall();
        MyAnsi ansi = g.getAnsi();
        //

        String hLine = Common.generateString('.', width );
        int r = y + 1;
        int c = x + 1;
        
        //ansi.cursor(r, c).a(hLine);
        ansi.appendString(hLine, r, c);
        //System.out.print(ansi.cursor(r, c).a(hLine));

        for (r++; r < y + height; r++)
        {
            //ansi.cursor(r, c).a(".");
            //ansi.cursor(r, c + width - 1).a(".");
            ansi.appendString(".", r, c);
            ansi.appendString(".", r, c + width - 1);
           // System.out.print(ansi.cursor(r, c).a("."));
           // System.out.print(ansi.cursor(r, c + width - 1).a("."));
        }
        System.out.print(ansi.appendString(hLine, r, c).reset());
        //  System.out.print(ansi.cursor(r, c).a(hLine).reset());
        System.out.flush();
    }

    public static void main(String[] args) throws IOException
    {

        //new CRectangle(3, 5, 20, 10).draw();

    }

    @Override
    public int getX()
    {
        // TODO Auto-generated method stub
        return x;
    }

    @Override
    public int getY()
    {
        return y;
    }
    
    public int getWidth()
    {
        return width;
    }
    
    public int getHeight()
    {
        return height;
    }


    @Override
    public boolean contains(int x, int y)
    {
        return x >= this.x && x <= (this.x + width) &&
               y >= this.y && y <= (this.y + width);
    }
}

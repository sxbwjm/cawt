package CLIGraphics;

import java.awt.geom.AffineTransform;

import org.fusesource.jansi.Ansi;

import static org.fusesource.jansi.Ansi.*;

public class MyAnsi extends Ansi
{
    protected Ansi _ansi = null;
    //protected Shape _clip = null;
    //AffineTransform _affTransform = null;
    protected Graphics2D _g2 = null;
    
    protected int _x = 0, _y = 0;
    protected int _w = 100;
    protected int _h = 50;
    
    /*
    public MyAnsi(int x, int y, int w, int h)
    {
        _x = x;
        _y = y;
        //_ansi = ansi();
        _w = w;
        _h = h;
    }
    */
    
    public MyAnsi(Graphics2D g2)
    {
        _g2 = g2;
    }
    
    
    public Ansi appendString(String str, int r, int c)
    {
        if(str == null || str.isEmpty())
        {
            return this;
        }
        // clip
        /*
        if(c >= _x && c < _w && r >= _y && r < _h)
        {
            int len = str.length();
            if(len > _w - c)
            {
                len = _w - c;
            }
            
            super.cursor(r, c).a(str.substring(0, len));
        }
        */
        c += _g2.getTransform().getTranslateX();
        r += _g2.getTransform().getTranslateY();
        
        for(int i=0; i<str.length(); i++)
        {
            if(_g2.getClip().contains(c, r))
            {
                super.cursor(r, c).a(str.charAt(i));
            }
            c++;
        }
            
      

        return this;
    }
    
    public void setClip(int x, int y, int w, int h)
    {
        _w = w;
        _h = h;
    }
    
}

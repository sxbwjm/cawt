package CLIGraphics;

public interface Shape
{
    public int getX();
    public int getY();
    public void draw(Graphics2D g);
    public boolean contains(int x, int y);
}

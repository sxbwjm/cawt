package CLIGraphics;
import java.awt.Stroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.geom.AffineTransform;





import org.fusesource.jansi.*;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

public class Graphics2D
{
   // protected int _canvasW = 0;
   // protected int _canvasH = 0;
    protected Shape _clip = null;
    protected Color _paintColor = null;
    
    AffineTransform _affTransform;
    public Graphics2D(int x, int y, int w, int h)
    {
        setClip(new CRectangle(x, y, w, h));
        _affTransform = new AffineTransform();
        AnsiConsole.systemInstall();
    }
    
    public Graphics2D()
    {
        this(0, 0, 0, 0);
    }
    
    public void clear()
    {
        System.out.println(ansi().eraseScreen().reset());
    }
    public void setPaint(Color c)
    {
        _paintColor = c;
    }
    
    public void translate(int x, int y)
    {
        _affTransform.translate(x, y);
    }
    
    public void setClip(int x, int y, int w, int h)
    {
        
        _clip = new CRectangle(x, y, w, h);
        //_canvasW = w;
        //_canvasH = h;
        //_ansi.setClipSize(w, h);
    }
    
    public Shape getClip()
    {
        return _clip;
    }
    
    public void setClip(Shape clip)
    {
        _clip = clip;
    }
    
    
    public Graphics2D create()
    {
        Graphics2D copy = new Graphics2D();
        copy.setClip(_clip);
        copy.transform(_affTransform);
        return copy;
    }
    
    public MyAnsi getAnsi()
    {
        //return new MyAnsi(_canvasW, _canvasH);
        return new MyAnsi(this);
    }
    
    public void fill(java.awt.geom.RoundRectangle2D rect)
    {
        
    }
    
    public void draw(Shape s)
    {
        s.draw(this);
    }
    
    public void fill(Shape s)
    {
        
    }
    
    public void setStroke(Stroke stroke)
    {
        
    }
    
    public void setFont(Font f)
    {
        
    }
    public void setColor(Color c)
    {
        
    }
    
    public AffineTransform getTransform()
    {
        return _affTransform;
    }
    
    public void transform(AffineTransform t)
    {
        _affTransform.concatenate(t);
    }
    
    public void drawString(String str, int x, int y)
    {
        //x += _affTransform.getTranslateX();
        //y += _affTransform.getTranslateY();
        System.out.println(getAnsi().appendString(str, y + 1, x + 1).reset());
    }
    
    public void dispose()
    {
    }
    
    
    static public void main(String[] argv)
    {
       
        
        Graphics2D g = new Graphics2D(5, 3, 50, 20);
        CRectangle r = new CRectangle(0, 0, 15, 10);
        g.draw(r);
        r = new CRectangle(20, 0, 15, 10);
        g.draw(r);
        CArrowLine a = new CArrowLine(10, 5, 20, 5);
        g.draw(a);
        
        g.drawString("test", 2, 5);
        
        System.out.println(ansi().cursor(20, 1));
        AnsiConsole.systemUninstall();
    }
}

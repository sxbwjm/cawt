package CLIGraphics;

import static org.fusesource.jansi.Ansi.ansi;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.Ansi.Color;
import org.fusesource.jansi.AnsiConsole;

import Common.Common;

public class CArrowLine implements Shape
{
    protected int x1 = 0, y1 = 0, x2 = 0, y2 = 0;
    
    public CArrowLine(int x1, int y1, int x2, int y2)
    {
        this.x1 = x1; this.y1 = y1; this.x2 = x2; this.y2 = y2;
    }
    
    @Override
    public int getX()
    {
        return x1;
    }

    @Override
    public int getY()
    {
        return y1;
    }

    @Override
    public void draw(Graphics2D g)
    {
        //AnsiConsole.systemInstall();
        MyAnsi ansi = g.getAnsi();
        //

        int r = y1 + 1;
        int c = x1 + 1;
        
        // print start symbol
       // ansi.cursor(r, c++).a("+");
        ansi.appendString("+", r, c);
        
        // then move in x direction
        int dx = x2 - x1;
        String xLine = "";
        if(dx > 3)
        {
            xLine = Common.generateString('-', dx - 3) + "+";
           // ansi.cursor(r, c).a(xLine);
            ansi.appendString(xLine, r, c);
            c += dx - 3;
        }
      


        // move in y direction then
        int dy = y2 - y1;
        if(dy > 0)
        {
            for (r++; r < y2 + 1; r++)
            {
                //ansi.cursor(r, c).a("|");
                ansi.appendString("|", r, c);
            }
            
            //ansi.cursor(r, c++).a("+");
            ansi.appendString("+", r, c++);
        }
        else if(dy < 0)
        {
            for (r--; r > y2 + 1; r--)
            {
                //ansi.cursor(r, c).a("|");
                ansi.appendString("|", r, c);
            }
            
            //ansi.cursor(r, c++).a("+");
            ansi.appendString("+", r, c++);
        }
        else
        {
          //ansi.cursor(r, c++).a("-");
          ansi.appendString("-", r, c++);
        }
         
        // print arrow
        //System.out.print(ansi.cursor(r, c).a('>').reset());
        System.out.print(ansi.appendString(">", r, c).reset());
      
        //ansi.reset();

        System.out.flush();
    }

    @Override
    public boolean contains(int x, int y)
    {
        return false;
    }

}

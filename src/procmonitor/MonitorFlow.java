package procmonitor;

import static org.fusesource.jansi.Ansi.ansi;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.fusesource.jansi.AnsiConsole;

import cawt.CFrame;
import cawt.CPanel;
import flow.FlowObject;
import flow.FlowPanel;
import Common.*;


public class MonitorFlow extends CPanel
{
    FlowPanel _flowPane = null;
    
    String _batchID = "";
    
    public MonitorFlow()
    {
        _flowPane = new FlowPanel();
        _flowPane.setSize(100, 30);
        _flowPane.setReadOnly(true);
        
        add(_flowPane);
        /*
        final JScrollPane scroll = new JScrollPane(_flowPane);
        _flowPane.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                if(e.getStateChange() == ItemEvent.SELECTED)
                {
                    FlowObject shape = (FlowObject)e.getItem();
                    RunJob job = (RunJob)shape.getJobData();
                    invokeMonitorListeners(job.runID);
                    scroll.scrollRectToVisible(shape.getBounds());
                }
            }
            
        });
        */
        
        //setLayout(new BorderLayout());
        //add(scroll, BorderLayout.CENTER);
    }
    
    /*
    public RunJob getSelectedJob()
    {
        FlowObject shape = getSelectedShape();
        if(shape != null)
        {
            return (RunJob)shape.getJobData();
        }
        
        return null;
    }
    
    public FlowObject getSelectedShape()
    {
        Object[] arr = _flowPane.getSelectedObjects();
        if(arr != null && arr.length > 0)
        {
            return (FlowObject)arr[0];
        }
        
        return null;
    }
    
    public void addMonitorListener(MonitorListener l)
    {
        _mListeners.add(l);
    }
    */
    public void setBatchID(String batchID)
    {
        _batchID =  batchID;
        _flowPane.setShapeList(createFlowShapes());
    }
    
    private Vector<FlowObject> createFlowShapes()
    {
       // System.out.println("creating shapes");
        String sql = "SELECT run_job_id as job_id, object_name, mzb_exec_id as exec_id, mzb_object_id as object_id, status, " +
                     "to_char(created_dt, 'YYYYMMDD HH24:MI:SS') as created_dt, " + 
                     "to_char(updated_dt, 'YYYYMMDD HH24:MI:SS') as updated_dt, " + 
                     "pattern, context " +
                     "FROM tmdm_run_job WHERE mzb_batch_id = " + _batchID;        
        
        //System.out.println("getting jobs");
        Vector<HashMap<String, String>> jobs = ServerPoolDB.getCurServer().getResultMap(sql); 
        //System.out.println("getting jobs done");
        Vector<FlowObject> shapes = new Vector<FlowObject>();
        
        for(HashMap<String, String> job : jobs)
        {
            FlowObject shape = new FlowObject(job.get("OBJECT_ID"));
            shape.setJobStatus(job.get("STATUS"));
            RunJob runJob = new RunJob(job.get("OBJECT_ID"), job.get("JOB_ID"), job.get("OBJECT_NAME"), job.get("EXEC_ID"), job.get("STATUS"),
                                       job.get("CONTEXT"), job.get("CREATED_DT"), job.get("UPDATED_DT"));
            shape.setJobData(runJob);
            shapes.add(shape);
        }
        //System.out.println("getting dependency");
        // dependency
        sql = "select b.mzb_object_id, b.mzb_parent_object_id from tmdm_run_job a " +
              "inner join tmdm_object_dependency b " +
              "on a.MZB_OBJECT_ID = b.mzb_object_id and (" +
              "   (a.pattern = b.pattern) or (a.pattern is null and b.pattern is null)) " +
              "where mzb_batch_id = " + _batchID + " order by b.mzb_object_id";
        Vector<Vector<String>> dps = ServerPoolDB.getCurServer().getResultVector(sql);
       // System.out.println("getting dependency done");
        for(Vector<String> dp : dps)
        {
            FlowObject child = null;
            FlowObject parent = null;
            for(FlowObject s : shapes)
            {
   
                if(s.jobID.equals(dp.get(0)))
                {
                    child = s;
                }
                else if(s.jobID.equals(dp.get(1)))
                {
                    parent = s;
                }
                    
            }
            
            if(child != null && parent != null)
            {
                child.addParent(parent);
            }
        }
        
        //System.out.println("creating shapes done");
        return shapes;
        
    }
    
    public void repaint()
    {
        _flowPane.repaint();
    }
    
    public void refresh()
    {
        Vector<FlowObject> shapes =  _flowPane.getShapeList();
        
        // flow won't be created if batch id is selected when batch status is 'B'
        // need to recreate it
        if(shapes == null || shapes.size() == 0)
        {
            _flowPane.setShapeList(createFlowShapes());
            repaint();
            return;
        }
        
        String sql = "SELECT run_job_id as job_id, object_name, mzb_exec_id as exec_id, mzb_object_id as object_id, status, " + 
                      "to_char(created_dt, 'YYYYMMDD HH24:MI:SS') as created_dt, " +
                      "to_char(updated_dt, 'YYYYMMDD HH24:MI:SS') as updated_dt, pattern, context " +
                "FROM tmdm_run_job WHERE mzb_batch_id = " + _batchID;        

        Vector<HashMap<String, String>> jobs = ServerPoolDB.getCurServer().getResultMap(sql);
        
        String selectedJobID = null;
        for(FlowObject s : shapes)
        {
            // if selected job is not set yet, set value as original selected job
            if(selectedJobID == null && s.isSelected())
            {
                selectedJobID = s.jobID;
            }
            //
            for(HashMap<String, String> job : jobs)
            {
                // update the job data
                if(s.jobID.equals(job.get("OBJECT_ID")))
                {
                    // RunJob runJob = new RunJob(job.get("OBJECT_ID"), job.get("JOB_ID"), job.get("OBJECT_NAME"), job.get("EXEC_ID"), job.get("STATUS"), job.get("CREATED_DT"), job.get("UDPATED_DT"));
                    RunJob runJob = (RunJob)s.getJobData();
                    runJob.execID = job.get("EXEC_ID");
                    runJob.status = job.get("STATUS");
                    runJob.updatedDate = job.get("UPDATED_DT");
                    runJob.context = job.get("CONTEXT");
                    //s.setJobData(runJob);
                    s.setJobStatus(job.get("STATUS"));
                    
                    // update selected job
                    if(s.getJobStatus().equals("I"))
                    {
                        selectedJobID = s.jobID;
                    }
                    break;
                }
            }
        }
        
        // invoke item change event even if the selection doesn't change
        _flowPane.selectShapeByID(selectedJobID);
        
        repaint();
    }
    
    public void clearData()
    {
        _flowPane.getShapeList().clear();
    }
    
    static public void main(String[] argv)
    {
        
        CFrame cframe = new CFrame(0, 0, 100, 50);

       // System.out.println("starting...");
        CPanel p = new CPanel();
        p.setSize(100, 50);
        cframe.add(p);
        MonitorFlow pane = new MonitorFlow();
        cframe.add(pane);
        pane.setLocation(0, 0);
      //  System.out.println("loading...");
        pane.setBatchID("3992");
       // pane.restart();
        cframe.setVisible(true);

        
        System.out.println(ansi().cursor(31, 1).a("done"));
        AnsiConsole.systemUninstall();
        ServerPoolDB.getCurServer().close();
        
    }
}

package procmonitor;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.*;
import java.util.Date;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import cawt.CPanel;

/**
 * Monitor abstract class has two functions:
 *   1. refresh it's contents
 *   2. invoke MonitorListener when it's item is selected, and pass it's item ID
 *      this probably can be done by ItemListener
 *      
 * The following functions need to be overwritten:
 *   - clearData()
 *   - refresh()
 *   
 * @author xiaobos
 *
 */
abstract public class Monitor extends CPanel
{
    protected int _timerSpeed = 5000;
   // protected Timer _timer = null;
    protected String _selectedItemID = "";
    
    //protected Vector<MonitorListener> _mListeners = new Vector<MonitorListener>();
    
    public Monitor()
    {
        ActionListener l = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                //DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                Date d1 = new Date();
                //String name = "";
                //System.out.println("refresh start:" + df.format(new Date()));
                refresh();
                Date d2 = new Date();
                //System.out.println(Monitor.this.getClass().getName() + ":" + (d2.getTime() -d1.getTime()));
            }
            
        };
        
        
      //  _timer = new Timer(_timerSpeed, l);
     //   _timer.setInitialDelay(0);
    }
    
    /*
    public void addMonitorListener(MonitorListener l)
    {
        _mListeners.add(l);
    }
    
    
    protected void invokeMonitorListeners(String itemID)
    {
        MonitorEvent event = new MonitorEvent(this, itemID);
        for(MonitorListener l : _mListeners)
        {
            l.itemChanged(event);
        }
    }
    */
    
    public void restart()
    {
       // _timer.restart();
    }
    
    public void stop()
    {
      //  _timer.stop();
    }
    
    abstract public void clearData();
    
    abstract public void refresh();
    
}

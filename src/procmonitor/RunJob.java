package procmonitor;

import java.awt.Color;

import flow.FlowData;


public class RunJob implements FlowData
{
    public String id = "";
    public String runID = "";
    public String name = "";
    public String execID = "";
    public String status = "";
    public String context = "";
    public String createdDate = "";
    public String updatedDate = "";
    
    private final String[] STATUS = {"R", "I", "S", "F", "H"};
    private final Color[] COLOR_BG = {Color.WHITE, Color.BLUE, Color.GRAY, Color.RED, Color.CYAN};
    private final Color[] COLOR_FG = {Color.BLACK, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE};
    private final Color COLOR_BG_DEFAULT = Color.WHITE;
    private final Color COLOR_FG_DEFAULT = Color.BLACK;
    
    public RunJob(String id, String runID, String name, String execID, String status, String context, String createdDate, String updatedDate)
    {
        this.id = id;
        this.runID = runID;
        this.name = name;
        this.execID = execID;
        this.status = status;
        this.context = context;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }

    @Override
    public String getDigestString()
    {
        return name;
    }

    @Override
    public String getDetailedString()
    {
        return "run_id=" + runID + " exec_id=" + execID + "\r\n" +
               "context=\r\n" + context.replace(",", "\r\n,");
    }

    @Override
    public Color getBackgroundRenderColor()
    {
        for(int i=0; i<STATUS.length; i++)
        {
            if(status.equals(STATUS[i]))
            {
                return COLOR_BG[i];
            }
        }
        
        return COLOR_BG_DEFAULT;
    }

    @Override
    public Color getForegroundRenderColor()
    {
        for(int i=0; i<STATUS.length; i++)
        {
            if(status.equals(STATUS[i]))
            {
                return COLOR_FG[i];
            }
        }
        
        return COLOR_FG_DEFAULT;
    }
}

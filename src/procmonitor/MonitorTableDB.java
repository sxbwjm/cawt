package procmonitor;

import javax.swing.table.TableModel;

import Common.*;
/**
 * Implement MonitorTable by mapping database table
 * A SQL string is passed, then it refresh it's content by fetching records from DB
 */
public class MonitorTableDB extends MonitorTable
{
    private String _sql = null;
    
    public MonitorTableDB()
    {
        super();
    }
    
    public void setSQL(String sql)
    {
        _sql = sql;
    }
    
    protected TableModel getNewTableModel()
    {
        if(_sql == null)
        {
            return null;
        }
        return ServerPoolDB.getCurServer().getResultTable(_sql);
    }
    
    
    
}

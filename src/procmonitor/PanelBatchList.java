package procmonitor;

import static org.fusesource.jansi.Ansi.ansi;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.fusesource.jansi.AnsiConsole;

import cawt.CFrame;
import cawt.CPanel;
import Common.*;

public class PanelBatchList extends CPanel
{
  //  private AutoSuggestComboBox _JF_procName;
   // private TextFieldWithHint _TF_batchID;
    
    private final Color TITLE_COLOR = Color.BLUE;
    private final int[] COL_WIDTH = {5, 20, 17, 17, 4};
    private final int[] COL_WIDTH_RUNNING_REQUEST = {80, 100, 200};
    private final int MAX_BATCH_NUM = 15;
    
    MonitorTableDB _PN_batchList = null;
    MonitorTableDB _PN_runningRequest = null;
    String _curBatchID = null;
    
    private boolean _autoFresh = false;
    
    public PanelBatchList()
    {
        super();
        
        buildPanel();
    }
    
    public void load()
    {
        //_JF_procName.setCandidates(getProcNameList());
        //loadRunningRequest();
        loadBatchList();
    }

    /*
    public void addMonitorListener(MonitorListener l)
    {
        _PN_batchList.addMonitorListener(l);
    }
    */
    private void buildPanel()
    {
        _PN_batchList = new MonitorTableDB();
        _PN_batchList.setSize(150, 30);
        add(_PN_batchList);
    }
    /*
    private void buildPanel()
    {
        JPanel panel = this; //new JPanel(new BorderLayout());
        panel.setLayout(new BorderLayout());
        panel.setPreferredSize(new Dimension(450, 300));
        panel.setBorder(BorderFactory.createBevelBorder(1));
        JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        
        JLabel label = new JLabel("latest " + MAX_BATCH_NUM + " batches");
        label.setForeground(TITLE_COLOR);
        panel2.add(label);
        panel2.add(buildProcNamePanel());

        
        JPanel panel3 = new JPanel(new BorderLayout());
        panel3.add(panel2, BorderLayout.NORTH);
        
        JTabbedPane tabPane = new JTabbedPane();
        _PN_batchList = new MonitorTableDB();
        panel3.add(_PN_batchList, BorderLayout.CENTER);
        tabPane.add("Batch List", panel3);

        panel3 = new JPanel(new BorderLayout());
        panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        
        JButton button = new JButton("STOP");
        button.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                stopRequest(_PN_runningRequest.getSelectedItemID());
            }
        }
        );;
        panel2.add(button);
        panel3.add(panel2, BorderLayout.NORTH);
        _PN_runningRequest = new MonitorTableDB();
        panel3.add(_PN_runningRequest, BorderLayout.CENTER);
        
        tabPane.add("Running Request", panel3);
        
        panel.add(tabPane, BorderLayout.CENTER);
        panel.add(buildControlPanel(), BorderLayout.SOUTH);
       
        _PN_batchList.setItemIDCol(0);
        _PN_batchList.setStatusCol(4);
        
    }
    */
    
    /*
    private JPanel buildProcNamePanel()
    {
        JPanel panel = new JPanel();
        FlowLayout layout = new FlowLayout(FlowLayout.LEFT);
        layout.setAlignment(FlowLayout.LEFT);
        panel.setLayout(layout);
        //panel.setMaximumSize(new Dimension(3000, 50));
        JLabel label = new JLabel("Process");
        label.setForeground(TITLE_COLOR);
       // panel.add(label);
       
        _JF_procName = new AutoSuggestComboBox();
        _JF_procName.setCaseSensitive(false);
        _JF_procName.setHintText("request name");
        _JF_procName.setPreferredSize(new Dimension(150, 20));
       
        ActionListener l = new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                loadBatchList();
                
            }
        };
        
        _JF_procName.getEditor().addActionListener(l);
        panel.add(_JF_procName);
        
        _TF_batchID = new TextFieldWithHint("batch id");
        _TF_batchID.setPreferredSize(new Dimension(50, 20));
        _TF_batchID.addActionListener(l);
        panel.add(_TF_batchID);
        
        /*
        JButton button = new JButton("filter");
        button.setName("load");
        ActionListener l = new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                loadBatchList();
                
            }
        };
        //ButtonListener l = new ButtonListener();
        button.addActionListener(l);
        
        panel.add(button);
        
        
        return panel;
    }
*/
    /*
    private JPanel buildControlPanel()
    {
        JPanel panel = new JPanel();
        FlowLayout layout = new FlowLayout(FlowLayout.LEFT);
        panel.setLayout(layout);
        JLabel label = new JLabel("Auto Reresh");
        label.setForeground(Color.MAGENTA);
        panel.add(label);
        final SwitchButton button = new SwitchButton("ON", "OFF");
        button.setPreferredSize(new Dimension(100, 25));
        //button.setFont(new Font("Arial", Font.PLAIN, 6));

        button.setSelected(true);
        button.addItemListener(new ItemListener()
        {

            @Override
            public void itemStateChanged(ItemEvent e)
            {
                if(button.isSelected())
                {
                    _autoFresh = true;
                    _PN_batchList.restart();
                }
                else
                {
                    _autoFresh = false;
                    _PN_batchList.stop();
                }
            }
            
        });
        panel.add(button);
        
        return panel;
    }
    */
    
    private Vector<String> getProcNameList()
    {
        String sql = "select request_name from MZPLUS_METABASE.tmdm_request";
        return ServerPoolDB.getCurServer().getSingleColumnVector(sql);
    }
    
    /*
    private void loadRunningRequest()
    {
        String sql = "select REQUEST_ID, REQUEST_NAME, DESCRIPTION " +
                     "from tmdm_request where STATUS_CODE = 'I' " +
                     "order by REQUEST_NAME";
        _PN_runningRequest.setColWidth(COL_WIDTH_RUNNING_REQUEST);
        _PN_runningRequest.setSQL(sql);
        _PN_runningRequest.restart();
    }
    */
    
    private void stopRequest(String selectedItemID)
    {
        String sql = "update tmdm_request set status_code = 'R' where request_id = '" + selectedItemID + "'";
        ServerPoolDB.getCurServer().execute(sql);
    }
    
    public void loadBatchList()
    {
        String procName = ""; //(String)_JF_procName.getSelectedItem();
        String batchID = ""; //_TF_batchID.getText();
        
        String sql = "SELECT mzb_batch_id as ID, request_name as name, to_char(start_ts, 'MM/DD HH24:MI:SS') as start_time, " +
                     "to_char(end_ts, 'MM/DD HH24:MI:SS') as end_time, " +
                     "status_code as STS FROM " +
                    "(" +
                    "SELECT a.*, b.request_name FROM tmdm_batch a inner join tmdm_request b " +
                    "on a.request_id = b.request_id ";
        
        // filter by process name
        if(procName != null && !procName.isEmpty())
        {
            sql += "WHERE b.request_name = '" + procName + "' ";
            if(batchID != null && !batchID.isEmpty())
            {
                sql += "and a.mzb_batch_id = '" + batchID + "' ";
            }
        }
        else if(batchID != null && !batchID.isEmpty())
        {
            sql += "where a.mzb_batch_id = '" + batchID + "' ";
        }
        
        
        
        sql += "ORDER BY mzb_batch_id DESC" +
               ") where rownum <= " + MAX_BATCH_NUM + " " + 
               "order by mzb_batch_id desc";

        _PN_batchList.setColWidth(COL_WIDTH);
        _PN_batchList.setSQL(sql);
    
        if(_autoFresh)
        {
            _PN_batchList.restart();
        }
        else
        {
            _PN_batchList.refresh();
        }
       
    }
    
    public void clearData()
    {
        _PN_batchList.clearData();
    }
    
    static public void main(String[] argv)
    {
        
        CFrame bframe = new CFrame(0, 0, 100, 30);
        CFrame fframe = new CFrame(0, 0, 100, 30);
      //  System.out.println("starting...");
        PanelBatchList pane = new PanelBatchList();
        pane.setSize(100, 30);
        bframe.add(pane);
        MonitorFlow p = new MonitorFlow();
        p.setSize(100, 30);
        fframe.add(p);
      //  System.out.println("loading...");
       // pane.setBatchID("3967");
       // pane.restart();
        bframe.setVisible(true);
        pane.load();
     
        
       System.out.print(ansi().cursor(40, 1).a("Enter Batch ID:"));
        System.out.flush();
        Scanner key = new Scanner(System.in);
 
            int id = key.nextInt();
           
            p.setBatchID("" + id);
            fframe.setVisible(true);
           
        key.close();
        System.out.println(ansi().cursor(40, 1).a(""));
        AnsiConsole.systemUninstall();
        ServerPoolDB.getCurServer().close();
    }  
}

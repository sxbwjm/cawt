package procmonitor;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import cawt.CTable;

/**
 * Abastract class: MonitorTable
 * 
 * Use JTable to implement Monitor
 * 
 * @author xiaobos
 *
 */
abstract public class MonitorTable extends Monitor
{
    protected CTable _table = null;
    protected int _itemIDCol = 0;
    protected int _statusCol = -1;
    protected String _selectedItemID = "";
    private int[]     _colWidth    = null;
    private boolean _isActive = true;
    
    public MonitorTable()
    {
        super();
        _table = new CTable();
        add(_table);
    }
    public void setSize(int w, int h)
    {
        super.setSize(w, h);
        _table.setSize(w, h);
    }
    @Override
    public void refresh()
    {
        TableModel m = getNewTableModel();
        if(m == null)
        {
            m = new DefaultTableModel();
        }

        _table.setModel(m);
        setColSize();
        _table.repaint();
      //  selectRowByItemID(_selectedItemID);
        
      
    }
    
    @Override
    public void clearData()
    {
        _table.setModel(new DefaultTableModel());
    }
    
    public  String getSelectedItemID()
    {
        return _selectedItemID;
    }
    
    
    public Vector<String> getRowDataByItemID(String itemID)
    {
        Vector<String> result = new Vector<String>();
        for(int r=0; r<_table.getRowCount(); r++)
        {
            if(itemID.equals((String)_table.getValueAt(r, _itemIDCol)))
            {
                for(int i=0; i<_table.getColumnCount(); i++)
                {
                    result.add((String)_table.getValueAt(r, i));
                }
            }
        }
        
        return result;
    }
    
    public void setItemIDCol(int idx)
    {
        _itemIDCol = idx;
    }
    
    public void setStatusCol(int idx)
    {
        _statusCol = idx;
        //MonitorTableCellRender render = new MonitorTableCellRender(_statusCol);
       // _table.setDefaultRenderer(Object.class, render);
    }
    
    public void setColWidth(int[] colWidth)
    {
        _colWidth = colWidth;
    }

    protected void setColSize()
    {
        if(_colWidth == null)
        {
            return;
        }
        /*
        for (int i = 0; (i < _colWidth.length) && (i < _table.getColumnCount()) ; i++)
        {
            _table.getColumnModel().getColumn(i)
                    .setPreferredWidth(_colWidth[i]);
        }
        */
        _table.setColWidth(_colWidth);
    }
    
    public int getRowCounts()
    {
        return _table.getRowCount();
    }
    
    abstract protected TableModel getNewTableModel();
}

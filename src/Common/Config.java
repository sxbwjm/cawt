package Common;

import java.awt.Dialog;
import java.io.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;

/**
 * Config: Common Configuration
 * 
 * @author xiaobos
 *
 */
public class Config
{

    public static enum ServerType
    {
        DEV, STG, PROD
    }

    public static final String SVN_REPOSITORY = "https://code.intra.infousa.com/svn/mzplus-amex/";
    public static final String TALEND_SRC_DIR = "/data/programs/mzp/";
    public static final String TALEND_GENERIC_SRC_DIR = "/data/programs/generic";

    public static String[] SVN_MANAGED_DIRS_PROJECT = {"configs", "scripts/perl", "scripts/shell", "scripts/sql", "scripts/sql/generic"};
    public static String[] SVN_MANAGED_DIRS_GENERIC = {"c", "config", "scripts/shell", "scripts/perl"};

    public static ServerType curEnv = ServerType.DEV;

    // ************constants that are not configurable by user****************
    public static String    defaultFontName               = "Baskerville";

    public static String    TALEND_SERVER_DEV             = "papdpstald01";
    public static String    TALEND_SERVER_STG             = "papdpstals01";
    public static String    TALEND_SERVER_PROD            = "stcdpstalp002l";
    

    public static String    DB_SERVER_DEV                 = "mzpamxtdb.intra.infousa.com";
    public static String    DB_SERVER_STG                 = "mzpamxsdb.intra.infousa.com";
    public static String    DB_SERVER_PROD                = "mzpamxpdb.intra.infousa.com";

    public static String    DB_SID_DEV                    = "mzpamx_test.intra.infousa.com";
    public static String    DB_SID_STG                    = "mzpamx_stage.intra.infousa.com";
    public static String    DB_SID_PROD                   = "mzpamx_prod.intra.infousa.com";

    public static String    DB_LOGIN_USER                 = "MZPLUS_METABASE";
    public static String    DB_LOGIN_PSWD                 = "Ea2tCoa2t";

    public static String    UPDATE_SERVER                 = "papdpstald01";

    public static String    WB_WS_URL                     = "http://dpservices:50002/WB.DDTWS/DDTService.asmx?WSDL";
    public static String    AD_DOMAIN                     = "intra.infousa.com";

    public static String    DIR_FORMAT                    = "fmt/";
    public static String    WK_DIR_TMP                    = "tmp/";
    public static String    DATA_DIR_TMPL                 = "/amex/data/";
    public static String    TMPL_FILE_TABLE_BUILD_REGULAR = "table_build_regular.tmpl";
    public static String    TMPL_FILE_TABLE_BUILD_RAW     = "table_build_raw.tmpl";
    public static String    TMPL_FILE_FLOW_GEN     = "flow_generate.tmpl";
    public static String    TMPL_FILE_FLOW_MIGRATE = "flow_migrate.tmpl";
    
    public static String   DIR_JAVA_JOB = "/data/programs/generic/java";
    
    public static String    WK_FILE_TMP_DOWNLOAD          = "download.tmp";
    public static String    WK_FILE_TMP_UPLOAD          = "upload.tmp";

    public static final String FILE_LIST_POSTFIX = "_FileList.cfg";
    
    public static String[]  PROJECT_LIST                  = { "amexintl",
            "amexsbs", "amexgpd"                         };
    
    /*
    private static String[] SVN_FAVORITE_DIR             = {
        "/data/programs/mzp/__PROJECT__/configs/",
        "/data/programs/mzp/__PROJECT__/scripts/shell/",
        "/data/programs/mzp/__PROJECT__/scripts/sql/"
        };
        */

    public static String[]  QUICK_ACCESS_NAME             = { "config",
            "process", "incoming", "arv_working", "shell","sql_gen", "log"         };
    private static String[] QUICK_ACCESS_PATH             = {
            "/data/programs/mzp/__PROJECT__/configs/",
            "/data/datafiles/mzp/__PROJECT__/process/",
            "/data/datafiles/mzp/__PROJECT__/incoming/",
            "/data/datafiles/mzp/__PROJECT__/archive/workfiles/",
            "/data/programs/mzp/__PROJECT__/scripts/shell/",
            "/data/programs/mzp/__PROJECT__/scripts/sql/generic/",
            "/data/programs/mzp/__PROJECT__/logs/"
            };
    public static String    configFile                    = "config.xml";

    // auto update
    public static String    curVersion                    = "2.2.1";
    public static String    updateFilePath                = "/home/xiaobos/AmexTool/";
    public static String    versionFileName               = "version";
    public static String    versionFilePath               = updateFilePath
                                                                  + versionFileName;
    public static String    jarName                       = "AmexTool.jar";
    public static String    DownloadPath                  = updateFilePath
                                                                  + jarName;

    // for java job export
    public static String TALEND_JOB_SERVER_DEV = "papdpstald01";
    public static String TAC_URL_DEV = "http://papdpstald01:8080/org.talend.administrator/";
    public static String TAC_PROJECT = "MZP_Amex";
    public static String TAC_LOGIN_USER = "webservice@infogroup.com";
    public static String TAC_LOGIN_PSWD = "t@dm!n";
    public static String TAC_REPO_BRANCH = "trunk";
    
    public static int TALEND_JOB_SERVER_PORT_DEV = 8002;
    public static String TALEND_JOB_SERVER_PROMPT = "talend> ";
    public static String JAVA_JOB_EXP_TMP_DIR = "/tmp/java_jar/";
    public static String JAVA_JOB_EXP_DEST_DIR = "/tmp/java_jar/";
    
    
    //
    
    public static int       MAX_LEN_FORMAT_FIELD          = 30;

    // ***** settings that will be loaded from user's config file ****

    // authencation
    public static String    loginUser                     = "";
    public static String    loginPswd                     = "";

    // work directory
    public static String    workDir                       = ".";

    public static String svnWorkDir = "C:\\Users\\xiaobos\\Documents\\projects\\1_amex\\svn";
    //
    public static String    curProject                    = PROJECT_LIST[0];

    /*
    public static String[] getFavoritePaths()
    {
        String[] paths = new String[SVN_FAVORITE_DIR.length];
        for(int i=0; i<SVN_FAVORITE_DIR.length; i++)
        {
            paths[i] = SVN_FAVORITE_DIR[i].replace("__PROJECT__", curProject);
        }
        
        return paths;
    }
    */
    
    public static String getQuickAccessPath(int idx)
    {
        return QUICK_ACCESS_PATH[idx].replace("__PROJECT__", curProject);
    }
    
    public static String getQuickAccessPath(String name)
    {
        for(int i=0; i<QUICK_ACCESS_NAME.length; i++)
        {
            if(QUICK_ACCESS_NAME[i].equals(name))
                return QUICK_ACCESS_PATH[i].replace("__PROJECT__", curProject);
        }
        
        return null;
    }

    public static String getCurrProjectSrcDir()
    {
        return TALEND_SRC_DIR + curProject;
    }
    
   
    /**
     * write setting to xml file
     * 
     * @throws Exception
     */
    public static void write() throws Exception
    {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder docBuilder;
        try
        {
            docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            //
            Element root = doc.createElement("Config");

            Node node = doc.createElement("loginUser");
            node.setTextContent(loginUser);
            root.appendChild(node);

            node = doc.createElement("loginPswd");
            node.setTextContent(loginPswd);
            root.appendChild(node);

            node = doc.createElement("workDir");
            node.setTextContent(workDir);
            root.appendChild(node);

            node = doc.createElement("curProject");
            node.setTextContent(curProject);
            root.appendChild(node);

            doc.appendChild(root);

            TransformerFactory transformerFactory = TransformerFactory
                    .newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(
                    "{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(configFile));
            transformer.transform(source, result);
        }
        catch (Exception e)
        {

        }
    }

    /**
     * read setting from xml file
     * 
     * @throws Exception
     */
    public static void readFromXML() throws Exception
    {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder docBuilder;
        docBuilder = docFactory.newDocumentBuilder();
        Document doc = docBuilder.parse(new File(configFile));

        NodeList nList = null;

        nList = doc.getElementsByTagName("loginUser");
        loginUser = nList.item(0).getTextContent();

        nList = doc.getElementsByTagName("loginPswd");
        loginPswd = nList.item(0).getTextContent();

        nList = doc.getElementsByTagName("workDir");
        if (nList != null && nList.getLength() > 0)
        {
            workDir = nList.item(0).getTextContent();
        }

        nList = doc.getElementsByTagName("curProject");
        if (nList != null && nList.getLength() > 0)
        {
            curProject = nList.item(0).getTextContent();
        }

    }
    
    /*
    static public void loadConfig()
    {
        File config = new File(Config.configFile);
        if(!config.exists())
        {
            GUISettings setting = new GUISettings();
            setting.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
            setting.setVisible(true);
        }
        
        try
        {
            Config.readFromXML();
        }
        catch(Exception e)
        {
            //e.printStackTrace();
        }
        
        File dir = new File(Config.WK_DIR_TMP);
        if(!dir.isDirectory())
        {
            dir.mkdir();
        }
        
    }
    */

    public static String getServerHostname(ServerType server)
    {

        switch(server)
        {
            case DEV:
                return TALEND_SERVER_DEV;
            case STG:
                return TALEND_SERVER_STG;
            case PROD:
                return TALEND_SERVER_PROD;
        }
        return null;
    }

}

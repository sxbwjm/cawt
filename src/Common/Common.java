package Common;

public class Common
{
    static public String generateString(char chr, int n)
    {
        if (n <= 0)
        {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++)
        {
            sb.append(chr);
        }

        return sb.toString();
    }
}

package Common;

/**
 * A pool of DB server
 * 
 * Design Pattern: Object Pool 
 * 
 * @author xiaobos
 *
 */
public class ServerPoolDB {

	static ServerDB _dev = null;
	static ServerDB _stg = null;
	static ServerDB _prod = null;
	
	
	public static ServerDB getServer(Config.ServerType type)
    {
        switch(type)
        {
        case DEV: return getDev();
        case STG: return getStg();  
        case PROD: return getProd();
        }
        
        return null;
    }
	
    public static ServerDB getCurServer()
    {
       return getServer(Config.ServerType.DEV);
    }
	
	private static ServerDB getDev()
	{
		if (_dev == null)
		{
			_dev = new ServerDB(Config.DB_SERVER_DEV, Config.DB_SID_DEV);
		}
		
		return _dev;
	}
	
	private static ServerDB getStg()
	{
		if (_stg == null)
		{
			_stg = new ServerDB(Config.DB_SERVER_STG, Config.DB_SID_STG);
		}
		
		return _stg;
	}
	private static ServerDB getProd()
	{
		if (_prod == null)
		{
			_prod = new ServerDB(Config.DB_SERVER_PROD, Config.DB_SID_PROD);
		}
		
		return _prod;
	}

}

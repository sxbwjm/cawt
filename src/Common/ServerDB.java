package Common;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class ServerDB
{
    private String     _server = null;
    private String     _sid    = null;
    private Connection _conn   = null;

    public ServerDB(String server, String sid)
    {
        _server = server;
        _sid = sid;
        //System.out.println("connecting");
        connect();
       // System.out.println("connected");
    }

    private void connect()
    {
        try
        {
           // System.out.println("load jdbc");
           // Class.forName("oracle.jdbc.driver.OracleDriver");
            DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
            String tns = "jdbc:oracle:thin:@" + _server + ":1521/" + _sid;
          //  System.out.println("load jdbc done");
            _conn = DriverManager.getConnection(tns, Config.DB_LOGIN_USER,
                    Config.DB_LOGIN_PSWD);
        }
        /*
        catch (ClassNotFoundException e)
        {
            System.out.println("No JDBC driver found");
            e.printStackTrace();
        }
        */
        catch (SQLException e)
        {
            System.out.println("Connection failed");
            e.printStackTrace();
        }

    }
    
    public void close()
    {
        try
        {
            if(_conn != null)
            {
                _conn.close();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        
        _conn = null;
    }
    
    public boolean poll()
    {
        return true;
    }
    public void execute(String sql)
    {
        Statement stmt;
        try
        {
            stmt = _conn.createStatement();
            stmt.executeQuery(sql);
            stmt.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    private ResultSet getResultSet(String sql)
    {
        Statement stmt;
        try
        {
            stmt = _conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            return rs;
        }
        catch (SQLException e)
        {
            
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    public Vector<Vector<String>> getResultVector(String sql)
    {
        Vector<Vector<String>> result = new Vector<Vector<String>>();
        ResultSet rs = getResultSet(sql);
        try
        {
            int colCount = rs.getMetaData().getColumnCount();
            while (rs.next())
            {
                Vector<String> row = new Vector<String>();

                for (int i = 1; i <= colCount; i++)
                {
                    row.add(rs.getString(i));
                }
                result.add(row);
            }
            rs.close();
            return result;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }
    
    public Vector<HashMap<String, String>> getResultMap(String sql)
    {
        Vector<HashMap<String, String>> result = new Vector<HashMap<String, String>>();
        ResultSet rs = getResultSet(sql);
        
        try
        {
            int colCount = rs.getMetaData().getColumnCount();

            // get data
            while (rs.next())
            {
                HashMap<String, String> row = new HashMap<String, String>();

                for (int i = 1; i <= colCount; i++)
                {
                    String value = rs.getString(i);
                    row.put(rs.getMetaData().getColumnName(i), value == null ? "" : value);
                }
                result.add(row);
            }
            rs.close();
            
            return result;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public TableModel getResultTable(String sql)
    {
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        ResultSet rs = getResultSet(sql);

        Vector<String> header = new Vector<String>();

        try
        {
            int colCount = rs.getMetaData().getColumnCount();

            // get column name
            for (int i = 1; i <= colCount; i++)
            {
                header.add(rs.getMetaData().getColumnName(i));
            }

            // get data
            while (rs.next())
            {
                Vector<Object> row = new Vector<Object>();

                for (int i = 1; i <= colCount; i++)
                {
                    row.add(rs.getString(i));
                }
                data.add(row);
            }
            rs.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }

        return new DefaultTableModel(data, header)
        {
            // read-only, but allow user copy values
            
            @Override
            public void setValueAt(Object oValue, int row, int nColumn)
            {
            }
            
            public boolean isCellEditable(int row, int column)
            {
                return true;
            }
        };
    }
    
    public Vector<String> getSingleColumnVector(String sql)
    {
        Vector<String> result = new Vector<String>();
        
        ResultSet rs = getResultSet(sql);
        try
        {
            while (rs.next())
            {
                result.add(rs.getString(1));
            }
            rs.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return result;
    }
    
    public Vector<String> getOraReservedWords()
    {
        String sql = "select keyword from V$RESERVED_WORDS where reserved = 'Y' order by 1";
      
        return getSingleColumnVector(sql);
    }


    public Vector<String> getKeyColumns()
    {
        String sql = "select distinct column_name "
                + "from MZPLUS_METABASE.tmdm_key_process_columns "
                + "where process_name = 'NAM_TABLE_DEF' " + "order by 1";
      
        return getSingleColumnVector(sql);
    }

    public Vector<String> getAuxKeyColumns()
    {
        String sql = "select distinct column_name "
                + "from MZPLUS_METABASE.tmdm_key_process_columns "
                + "where process_name like 'NAM_TABLE_DEF_AUX%' " + "order by 1";
      
        return getSingleColumnVector(sql);
    }
    
    public Vector<Vector<String>> getAllKeyColumnAndAlias()
    {
        String sql = "select distinct column_name, column_name column_alias "
                + "from MZPLUS_METABASE.tmdm_key_process_columns "
                + "where process_name = 'NAM_TABLE_DEF' or "
                + "process_name like 'NAM_TABLE_DEF_AUX%' "
                + "union "
                + "select column_name, column_alias "
                + "from MZPLUS_METABASE.tmdm_key_process_column_alias "
                + "where process_name = 'NAM_TABLE_DEF' " 
                + "order by 1, 2";

        return getResultVector(sql);
    }

    /**
     * Read key column alias into a map.
     * 
     * @return
     */
    public Map<String, Vector<String>> getKeyColumnAlias2()
    {
        Map<String, Vector<String>> result = new HashMap<String, Vector<String>>();

        String sql = "select column_name, column_alias "
                + "from MZPLUS_METABASE.tmdm_key_process_column_alias "
                + "where process_name = 'NAM_TABLE_DEF' " + "order by 1, 2";

        ResultSet rs = getResultSet(sql);

        String prevColumn = null;
        Vector<String> aliases = null;

        try
        {
            while (rs.next())
            {
                String curColumn = rs.getString(1);
                // key changed
                if (!curColumn.equals(prevColumn))
                {
                    if (prevColumn != null)
                    {
                        result.put(prevColumn, aliases);
                    }

                    aliases = new Vector<String>();
                }

                aliases.add(rs.getString(2));

                prevColumn = curColumn;
            }

            // add the final key
            result.put(prevColumn, aliases);
            rs.close();

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return result;
    }
    
    static public String escapeSQL(String sql)
    {
        return sql == null ? sql : sql.replace("'", "''");
    }

}

package flow;


import java.awt.Color;

public interface FlowData
{
    public String getDigestString();
    public String getDetailedString();
    public Color getBackgroundRenderColor();
    public Color getForegroundRenderColor();
}

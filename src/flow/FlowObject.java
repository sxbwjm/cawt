package flow;

import static org.fusesource.jansi.Ansi.ansi;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

import CLIGraphics.Graphics2D;
import CLIGraphics.CRectangle;

import java.awt.Paint;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.RoundRectangle2D;
import java.io.Serializable;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.util.Hashtable;
import java.util.Vector;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;

public class FlowObject extends CRectangle implements FlowShape
{
    private static final long serialVersionUID = 1L;
    static public final int WIDTH = 20;
    static public final int HEIGHT = 5;
    private final int ARC = 20;
    private final Color COLOR_BG_DEFAULT = Color.WHITE;
    private final Color COLOR_FG_DEFAULT = Color.BLACK;
    private final Color COLOR_BD_DEFAULT = Color.BLACK;
  //  private final transient  Stroke STROKE_DEFAULT = new BasicStroke(1);
    //private final transient  Stroke STROKE_SELECTED = new BasicStroke(3);
    
    private final int STROKE_DEFAULT = 1;
    private final int STROKE_SELECTED = 3;
    
   // private final Color COLOR_BG_SELECTED = Color.WHITE;
   // private final Color COLOR_FG_SELECTED = Color.BLACK;
    private final Color COLOR_BD_SELECTED = Color.MAGENTA;
    
   
    private Color _fillColor = COLOR_BG_DEFAULT;
    private Color _fontColor = COLOR_FG_DEFAULT;
    private Color _borderColor = COLOR_BD_DEFAULT;
    private int _stroke = STROKE_DEFAULT;
    
    private boolean _isSelected = false;
    //private static int _idSeq = 100000;
    

    
    
    //private FlowJob _flowJob = null;
    
    private String LABEL_FONT =  "LucidaGrande";
    
    public String jobID = "";
    //public String jobName = "";
    public String _jobStatus = "";
    
    private FlowData _jobData = null;
    
    private Vector<FlowObject> _parents = new Vector<FlowObject>();
    private Vector<FlowObject> _children = new Vector<FlowObject>();
    
    
    public FlowObject(String id)
    {
        super(0, 0,  WIDTH, HEIGHT);

        jobID = id;
        //jobName = name;
        
    }
    
    /*
    static String getNextID()
    {
        return "" + (_idSeq++);
    }
    */
    
    public void setJobData(FlowData data)
    {
        _jobData = data;
    }
    
    public FlowData getJobData()
    {
        return _jobData;
    }
    
    public void setJobStatus(String status)
    {
        _jobStatus = status;
    }
    
    public String getJobStatus()
    {
        return _jobStatus;
    }
    
    @Override
    public void setSelected(boolean value)
    {
        _isSelected = value;
    }
    
    @Override
    public boolean isSelected()
    {
        return _isSelected;
    }
    
    
    public void addParent(FlowObject parent)
    {
        if(this._parents.contains(parent))
        {
            return;
        }
        
        this._parents.add(parent);
        parent._children.add(this);
    }
    
    public Vector<FlowObject> getParents()
    {
        return _parents;
    }
    
    public Vector<FlowObject> getChildren()
    {
        return _children;
    }
    
    public void removeParent(FlowObject parent)
    {
        this._parents.remove(parent);
        parent._children.remove(this);
    }
    
    public void removeAllParents()
    {
        for(FlowObject p : _parents)
        {
            p._children.remove(this);
        }
        
        _parents.clear();
        
    }
    
    
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof FlowObject))
        {
            return false;
        }
        FlowObject s = (FlowObject)obj;
        return jobID.equals(s.jobID);
    } 
    
    
    public void paintComponent(Graphics2D g2)
    {
        Graphics2D g2copy = (Graphics2D)g2.create();
     
        // draw shape
        if(_isSelected)
        {
            //_fillColor = COLOR_BG_SELECTED;
            //_fontColor = COLOR_FG_SELECTED;
            _borderColor = COLOR_BD_SELECTED;
            _stroke = STROKE_SELECTED;
        }
        else
        {
            _borderColor = COLOR_BD_DEFAULT;
            _stroke = STROKE_DEFAULT;
        }
        // decide color
        _fillColor = COLOR_BG_DEFAULT;
        _fontColor = COLOR_FG_DEFAULT;
   
       
       if(_jobData != null)
       {
           _fillColor = _jobData.getBackgroundRenderColor();
           _fontColor = _jobData.getForegroundRenderColor();
       }
        
        // fill
        g2copy.setPaint(_fillColor);
        g2copy.fill(this);
        
        // draw shape border
        g2copy.setStroke(new BasicStroke(_stroke));
        g2copy.setPaint(_borderColor);
        g2copy.draw(this);

        // draw text

        Font f = new Font(LABEL_FONT, Font.BOLD, 10);
        g2copy.setFont(f);
        g2copy.setColor(_fontColor);
        
        AffineTransform oldTran = g2copy.getTransform();
        double scaleX = oldTran.getScaleX();
        double scaleY = oldTran.getScaleY();
       // this.x /= scaleX;
       // this.y /= scaleY;
       // this.width /= scaleX;
        //this.height /= scaleY;
        
        
        
        AffineTransform newTran = new AffineTransform();
        newTran.setToIdentity();
        newTran.translate(this.x, this.y);
        g2copy.transform(newTran);
        
        
        drawTextCenter(g2copy, jobID, 1);
        //drawTextWrap(g2copy, "Set_extract_parameters", 0, 2, WIDTH - 2);
        if(_jobData !=null)
        {
            String str = _jobData.getDigestString();
            if(str != null && !str.isEmpty())
            {
                drawTextWrap(g2copy, _jobData.getDigestString(), 0, 2, WIDTH - 4);
            }
           // drawTextCenter(g2copy, _jobData.getDigestString(), 50);
            if(_isSelected)
            {
                g2copy.setColor(Color.GRAY);
                //g2copy.drawString(_jobData.getDetailedString(), 0, HEIGHT + 15);
                drawTextWithLineBreaks(g2copy, _jobData.getDetailedString(), 0, HEIGHT + 15);
                //drawTextWrap(g2copy, _jobData.getDetailedString(), 0, HEIGHT +10, 500);
            }
        }
        
        g2copy.dispose();
        
    }
    
    private void drawTextCenter(Graphics2D g2, String str, int y)
    {
       // FontMetrics fm = g2.getFontMetrics();
       // int x = (WIDTH - fm.stringWidth(str)) / 2;
        int x = (WIDTH - str.length() ) / 2;
        g2.drawString(str, x, y);
    }
    
    private void drawTextWithLineBreaks(Graphics2D g2, String str, int x, int y)
    {
        String[] lines = str.split("\r\n");
        //FontMetrics fm = g2.getFontMetrics();
      //  int ySpace = fm.getHeight() + 5;
        int ySpace = 1;
        for(String line : lines)
        {
            g2.drawString(line, 0, y);
            y += ySpace;
        }
    }
    
    private void drawTextWrap(Graphics2D g2, String str, int x, int y, int breakWidth)
    {
        int r = 0;
        while(r * breakWidth < str.length())
        {
            int start = r * breakWidth;
            int end =  (r+1) * breakWidth;
            if(end > str.length())
            {
                end = str.length();
            }
            drawTextCenter(g2, str.substring(start, end), y + r);
            r++;
        }
        
        /*
        Hashtable<TextAttribute, Object> map =
                new Hashtable<TextAttribute, Object>();
      

             map.put(TextAttribute.FAMILY, "Serif");
             map.put(TextAttribute.SIZE, new java.lang.Float(18.0));
             
             AttributedString attrStr = new AttributedString(str);
             
             AttributedCharacterIterator paragraph = attrStr.getIterator();
             int paragraphStart = paragraph.getBeginIndex();
             int paragraphEnd = paragraph.getEndIndex();
             FontRenderContext frc = g2.getFontRenderContext();
             LineBreakMeasurer lineMeasurer = new LineBreakMeasurer(paragraph, frc);
             
             // Set break width to width of Component.
             //float breakWidth = (float)WIDTH;
             float drawPosY = y;
             // Set position to the index of the first character in the paragraph.
             lineMeasurer.setPosition(paragraphStart);
      
             // Get lines until the entire paragraph has been displayed.
             while (lineMeasurer.getPosition() < paragraphEnd) {
      
                 // Retrieve next layout. A cleverer program would also cache
                 // these layouts until the component is re-sized.
                 TextLayout layout = lineMeasurer.nextLayout(breakWidth);
      
                 // Compute pen x position. If the paragraph is right-to-left we
                 // will align the TextLayouts to the right edge of the panel.
                 // Note: this won't occur for the English text in this sample.
                 // Note: drawPosX is always where the LEFT of the text is placed.
                 float drawPosX = layout.isLeftToRight()
                     ? 0 : breakWidth - layout.getAdvance();
      
                 FontMetrics fm = g2.getFontMetrics();
                  drawPosX = (breakWidth - layout.getAdvance()) / 2;
                 // Move y-coordinate by the ascent of the layout.
                 drawPosY += layout.getAscent();
      
                 // Draw the TextLayout at (drawPosX, drawPosY).
                 layout.draw(g2, drawPosX, drawPosY);
      
                 // Move y-coordinate in preparation for next layout.
                 drawPosY += layout.getDescent() + layout.getLeading();
             }
             */

    }
    
    static public void main(String[] argv)
    {
        //AnsiConsole.systemUninstall();
        
        Graphics2D g = new Graphics2D(0, 0, 100, 10);
        FlowObject o = new FlowObject("111");
        o.paintComponent(g);
        
        o = new FlowObject("222");
        o.x = 30;
        o.y = 0;
        o.paintComponent(g);
       
        System.out.println(ansi().cursor(20, 1));
        AnsiConsole.systemUninstall();
    }

}

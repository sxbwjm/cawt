package flow;


import java.awt.Rectangle;
import java.util.Vector;

public class FlowShapeControl
{
    private final int xSpace = 4;
    private final int ySpace = 1;
    private final int xMargin = 1;
    private final int yMargin = 2;
    
    Vector<FlowObject> _shapeList = new Vector<FlowObject>();
    
    public FlowShapeControl(Vector<FlowObject> shapeList)
    {
        _shapeList = shapeList;
    }
    
    public void arrangeShapes()
    {
        Vector<Vector<FlowObject>> shapeTree = new Vector<Vector<FlowObject>>();
        Vector<FlowObject> shapesInColumn = null;
        //Vector<ShapeProcessFlow> objectsCopy = (Vector<ShapeProcessFlow>)_shapeList.clone();
        
        Vector<FlowObject> processedShapes = new Vector<FlowObject>();
        
        int watchDog = 1000;
        while(_shapeList.size() > 0)
        {
            // prevent from dead loop
            if(watchDog-- < 0)
            {
                break;
            }
            
            // save jobs that will be put in same column
            shapesInColumn = new Vector<FlowObject>();
            
            // select jobs whose parents have been all processed
            for(int i=0; i<_shapeList.size(); i++)
            {
                FlowObject j = _shapeList.get(i);

                // test if all parents have been processed
                boolean hasParent = false;
                for(FlowObject p : j.getParents())
                {
                    if(!processedShapes.contains(p))
                    {
                        hasParent = true;
                        break;
                    }
                }
                if(!hasParent)
                {
                    shapesInColumn.add(j);
  
                  
                }
            }
            
            
            // remove selected jobs from job list and added to processed jobs
            for(FlowObject j : shapesInColumn)
            {
                _shapeList.remove(j);
                processedShapes.add(j);
            }
            
            
            shapeTree.add(shapesInColumn);
        }
        
        
        int curX = xMargin;
        int curY = 0;
       
        
        // place job shape column by column
        Vector<FlowObject> preCol = null;
        Vector<FlowObject> processed = null;
        for(Vector<FlowObject> shapesCol : shapeTree)
        {
            curY = yMargin;
            processed = new Vector<FlowObject>();
            
            // first column: no parents
            if(preCol == null)
            {
                for(FlowObject job : shapesCol)
                {
                    job.x = curX;
                    job.y = curY;
                    processed.add(job);
                    _shapeList.add(job);
                    curY += ySpace + FlowObject.HEIGHT;
                }
            }
            else
            {
                // try to place shapes in same order with their parents
                for(FlowObject p : preCol)
                {
                    // y starts from parent's y
                    if(curY < p.getY())
                    {
                        curY = (int)p.getY();
                    }
                    
                    for(FlowObject job : shapesCol)
                    {
                        if(!processed.contains(job) && job.getParents().contains(p))
                        {
                            job.x = curX;
                            job.y = curY;
                            processed.add(job);
                            _shapeList.add(job);
                            curY += ySpace + FlowObject.HEIGHT;
                        }
                    }
                }
                
                // 
            }
          
            curX += xSpace + FlowObject.WIDTH;
            preCol = processed;
        }
        
    }
    
    public Rectangle calculateBounds()
    {
        Rectangle rec = new Rectangle(0, 0, 0, 0);
      
        for(FlowObject s : _shapeList)
        {
            if(s.x + s.width > rec.width)
            {
                rec.width = (int)(s.x + s.width);
            }
            
            if(s.y + s.height > rec.height)
            {
                rec.height = (int)(s.y + s.height);
            }
        }
        
        rec.width += xMargin;
        rec.height += yMargin;
        
        return rec;
    }
    
    public void moveShape(FlowObject s, int x, int y)
    {
        s.x = x;
        s.y = y;
    }

}

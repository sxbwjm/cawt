package flow;


import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import CLIGraphics.CArrowLine;
import CLIGraphics.Graphics2D;

public class FlowLink extends CArrowLine implements FlowShape
{
    private boolean _isSelected = false;
    
    private FlowShape _fromShape = null;
    private FlowShape _toShape = null;
    
    private final Stroke STROKE_DEFAULT = new BasicStroke(2);
    private final Stroke STROKE_SELECTED = new BasicStroke(3);
    private final Color COLOR_DEFAULT = Color.BLACK;
    private final Color COLOR_SELECTED = Color.BLUE;
    
    public FlowLink()
    {
        super(0, 0, 0, 0);
    }
    public FlowLink(int x1, int y1, int x2, int y2)
    {
        super(x1, y1, x2, y2);
    }
    public void setLine(int x1, int y1, int x2, int y2)
    {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }
    public void setConnectedShapes(FlowShape from, FlowShape to)
    {
        _fromShape = from;
        _toShape = to;
    }
    
    public FlowShape getFromShape()
    {
        return _fromShape;
    }
    
    public FlowShape getToShape()
    {
        return _toShape;
    }
   
    @Override
    public void setSelected(boolean value)
    {
        _isSelected = value;
    }
    
    @Override
    public boolean isSelected()
    {
        return _isSelected;
    }
    
    public void paintComponent(Graphics2D g2)
    {
        Graphics2D g2copy = (Graphics2D)g2.create();
        if(_isSelected)
        {
            g2copy.setStroke(STROKE_SELECTED);
            g2copy.setColor(COLOR_SELECTED);
        }
        else
        {
            g2copy.setStroke(STROKE_DEFAULT);
            g2copy.setColor(COLOR_DEFAULT);
        }
        g2copy.draw(this);
        Polygon arrowHead = new Polygon();  
        arrowHead.addPoint( 0,0);
        arrowHead.addPoint( -5, -10);
        arrowHead.addPoint( 5,-10);
        
        AffineTransform tx = new AffineTransform();
        tx.setToIdentity();
        double angle = Math.atan2(y2 - y1, x2 - x1);
        tx.translate(x2, y2);
        tx.rotate((angle-Math.PI/2d));  
        g2copy.transform(tx);
       // g2copy.fill(arrowHead);
        
      
        
    }

}

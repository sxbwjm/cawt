package flow;


import static org.fusesource.jansi.Ansi.ansi;

import java.awt.*;

import CLIGraphics.Graphics2D;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.RoundRectangle2D;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.fusesource.jansi.AnsiConsole;

import cawt.CFrame;
import cawt.CPanel;


public class FlowPanel extends CPanel implements ItemSelectable
{
    private Vector<FlowObject> _shapeList = null;
    FlowShapeControl _shapesControl = null;
    private Vector<FlowLink> _links = new Vector<FlowLink>();
    FlowLink _connectLine = new FlowLink();
    
    private Vector<ItemListener> _itemListeners = new Vector<ItemListener>();
    
    private int _panelWidth = 0;
    private int _panelHeight = 0;
    
    private FlowObject _selectedShape = null;
    private FlowLink _selectedLink = null;
    private Point _dragStartPoint = null;
    private Point _shapeStartPoint = null;
    private Point _connectingFromPoint = null;
    private Point _connectingToPoint = null;
    
    private int _idSeq = 100000;
    
    private boolean _readOnly = false;
    
    private double _zoom = 1.0;
    private double ZOOM_STEP = 0.05;
    
    private final double ZOOM_MIN = 0.3;
    private final double ZOOM_MAX = 3.0;

    
    public FlowPanel()
    {
        this(new Vector<FlowObject>());
    }
    
    public FlowPanel(Vector<FlowObject> objects)
    {
        
        setShapeList(objects);
    }
    
    public void setReadOnly(boolean value)
    {
        _readOnly = value;
    }
    
    public boolean isReadOnly()
    {
        return _readOnly;
    }
    
    public void removeSelectedShape()
    {
        if(_selectedShape != null)
        {
            // remove dependency first
            for(FlowObject s : _shapeList)
            {
                s.removeParent(_selectedShape);
            }
            
            // this is necessary because it's bidirection linked
            _selectedShape.removeAllParents();
            
            // remove selected shape then
            _shapeList.remove(_selectedShape);
            
            rebuildLinks();
            repaint();
        }
        
        if(_selectedLink != null)
        {
            FlowObject parent = (FlowObject)_selectedLink.getFromShape();
            FlowObject child = (FlowObject)_selectedLink.getToShape();
            child.removeParent(parent);
            
            rebuildLinks();
            repaint();
            
        }
    }
    
    public void setShapeList(Vector<FlowObject> objects, boolean arrangeLayout)
    {
        if(objects == null)
        {
            _shapeList = new Vector<FlowObject>();
        }
        else
        {
            _shapeList = objects;
        }
        
        for(FlowObject s : _shapeList)
        {
            if(s.isSelected())
            {
                _selectedShape = s;
                break;
            }
        }
        _shapesControl = new FlowShapeControl(_shapeList);
        if(arrangeLayout)
        {
            _shapesControl.arrangeShapes();
        }
        rebuildLinks();
        repaint();
    }
    
  
    
    public void setShapeList(Vector<FlowObject> objects)
    {
        setShapeList(objects, true);
    }
    
    public Vector<FlowObject> getShapeList()
    {
        return _shapeList;
    }
    
    public void paintComponent(Graphics2D g) {

        //super.paintComponent(g);
        Rectangle rec = _shapesControl.calculateBounds();
        _panelWidth = (int)(rec.width * _zoom);
        _panelHeight = (int)(rec.height * _zoom);
        if(_panelWidth > 0 && _panelHeight > 0)
        {
           //setPreferredSize(new Dimension(_panelWidth, _panelHeight));
        }

        Graphics2D g2 = g.create();
        //AffineTransform newTran = new AffineTransform();
        //newTran.setToIdentity();
        //newTran.scale(zoom/100, zoom/100);
        //g2.transform(newTran);
       // g2.scale(_zoom, _zoom);
        
       drawShapes(g2);
        drawLinks(g2);
        //drawConnectLine(g2);

        //revalidate();
        

    }
    
    private void drawShapes(Graphics2D g2) {

        for(FlowObject s : _shapeList)
        {
            s.paintComponent(g2);
        }
        
    }
    
    private void drawLinks(Graphics2D g2) {

        for(FlowLink s : _links)
        {
            s.paintComponent(g2);
        }
        
    }
    
    
    private void rebuildLinks()
    {
        _links.clear();
        for(FlowObject to : _shapeList)
        {
            for(FlowObject from : to.getParents())
            {
                int startX = from.x + from.width;
                int startY = from.y + from.height / 2;
                int endX = to.x;
                int endY = to.y + to.height / 2;
               // System.out.println(String.format("%s(%d,%d,%d,%d)", from.jobID, startX, startY, endX, endY));
                FlowLink link = new FlowLink(startX, startY, endX, endY);
                link.setConnectedShapes(from, to);
                _links.add(link);
            }
        }
    }
    
    private void drawConnectLine(Graphics2D g2)
    {
        if(_connectingFromPoint != null && _connectingToPoint != null)
        {
            _connectLine.setLine(_connectingFromPoint.x, _connectingFromPoint.y, _connectingToPoint.x, _connectingToPoint.y);
            _connectLine.draw(g2);
        }
           // g2.drawLine(_connectingFromPoint.x, _connectingFromPoint.y, _connectingToPoint.x, _connectingToPoint.y);
    }
    
    public void selectShapeByID(String ID)
    {
        for(FlowObject s : _shapeList)
        {
            if(s.jobID.equals(ID))
            {
                selectShape(s);
                return;
            }
        }
    }
    
    private void selectShape(FlowObject s)
    {
        deselectShape();
        s.setSelected(true);
        _selectedShape = s;
        fireItemListener(true);
    }
    
    private void deselectShape()
    {
        // deselect the shape first
        if(_selectedShape != null)
        {
            _selectedShape.setSelected(false);
            fireItemListener(false);
            _selectedShape = null;
        }
    }
    
    private void deselectLink()
    {
        if(_selectedLink != null)
        {
            _selectedLink.setSelected(false);
            _selectedLink = null;
        }
    }
    private void selectLink(FlowLink l)
    {
        deselectLink();
        _selectedLink = l;
        _selectedLink.setSelected(true);
    }
    
    private Point getTransformedPoint(Point p)
    {
        return new Point((int)(p.x / _zoom), (int)(p.y / _zoom));
    }
    
   
    
    public void setStartID(int id)
    {
        _idSeq = id;
    }
    
    public String getNextID()
    {
        return _idSeq++ + "";
    }
    
    public String getCurID()
    {
        return _idSeq + "";
    }
    
    private void addShape(Point loc)
    {
        FlowObject s = new FlowObject(getNextID());

        s.x = loc.x;
        s.y = loc.y;
        
        
        _shapeList.add(s);
        selectShape(s);
    }
    
    private void addLink(FlowObject from, FlowObject to)
    {
        to.addParent(from);
        rebuildLinks();
    }
    
    private void fireItemListener(boolean isSelected)
    {
        int state = isSelected ? ItemEvent.SELECTED : ItemEvent.DESELECTED;
        
        ItemEvent e = new ItemEvent(this, ItemEvent.ITEM_STATE_CHANGED, _selectedShape, state);
        for(ItemListener l : _itemListeners)
        {
            l.itemStateChanged(e);
        }
    }
    
    @Override
    public void addItemListener(ItemListener arg0)
    {
        _itemListeners.add(arg0);
    }

    @Override
    public Object[] getSelectedObjects()
    {
        if(_selectedShape != null)
        {
            return new Object[]{_selectedShape};
        }
        return null;
    }

    @Override
    public void removeItemListener(ItemListener arg0)
    {
        _itemListeners.remove(arg0);
    }

   
    static public void main(String[] argv)
    {
         FlowData data1 = new FlowData()
        {
           
            @Override
            public String getDigestString()
            {
                return "object1";
            }

            @Override
            public String getDetailedString()
            {
                return "object1\r\nthis is object1";
            }

            @Override
            public Color getBackgroundRenderColor()
            {
                return Color.RED;
            }

            @Override
            public Color getForegroundRenderColor()
            {
                return Color.WHITE;
            }
            
        };
        
        FlowData data2 = new FlowData()
        {
           
            @Override
            public String getDigestString()
            {
                return "object2";
            }

            @Override
            public String getDetailedString()
            {
                return "object2 long test this is object2";
            }

            @Override
            public Color getBackgroundRenderColor()
            {
                return Color.BLUE;
            }

            @Override
            public Color getForegroundRenderColor()
            {
                return Color.WHITE;
            }
            
        };
        Vector<FlowObject> objects1 = new Vector<FlowObject>();
        Vector<FlowObject> objects2 = new Vector<FlowObject>();
        FlowObject j1 = new FlowObject("1");
        j1.setJobData(data1);
        objects1.add(j1);
        FlowObject j2 = new FlowObject("2");
        j2.setJobData(data2);
        objects1.add(j2);
        FlowObject j3 = new FlowObject("3");
        j3.setJobData(null);
        objects2.add(j3);
       j2.addParent(j1);
       j3.addParent(j1);
        
        
        CFrame cframe = new CFrame(0, 0, 100, 100);
        FlowPanel pane = new FlowPanel(objects1);
        pane.setSize(50, 10);
        pane.setLocation(5, 10);
        cframe.add(pane);
        
        FlowPanel pane2 = new FlowPanel(objects2);
        pane2.setSize(30, 10);
        pane2.setLocation(31, 10);
        
        cframe.add(pane2);
        
        cframe.setVisible(true);
       //pane.repaint();
        
        System.out.print(ansi().cursor(30, 1));
        AnsiConsole.systemUninstall();
    }

   
}

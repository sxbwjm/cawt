package flow;
import CLIGraphics.Graphics2D;

public interface FlowShape
{
    public void draw(Graphics2D g2);
    public void setSelected(boolean value);
    public boolean isSelected();
}

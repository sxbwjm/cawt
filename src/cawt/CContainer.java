package cawt;

import java.util.ArrayList;

import CLIGraphics.Graphics2D;

public class CContainer extends Component
{
    ArrayList<Component> _children = new ArrayList<Component>();
    public void add(Component c)
    {
        _children.add(c);
    }
    
    public void paintChildren(Graphics2D g)
    {
        for(Component c : _children)
        {
            c.paint(g);
        }
    }
    
    public void paintComponent(Graphics2D g)
    {
        
    }
    
    
    @Override
    public void paint(Graphics2D g)
    {
        super.paint(g);
        g.translate(getX(), getY());
        g.setClip(getClip());
        paintComponent(g);
        paintChildren(g);
        g.translate(-getX(), -getY());
    }
    
    @Override
    public void dispatchEvent(CAWTEvent e)
    {
        proecessEvent(e);
    }
    

    
    
}

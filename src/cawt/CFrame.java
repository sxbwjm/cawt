package cawt;

import java.awt.Dimension;

import CLIGraphics.Graphics2D;

public class CFrame extends CContainer
{
    protected Graphics2D _graphics2D = null;
    protected boolean _visible = false;
   // protected int _x = 0, _y = 0, _w = 0, _h = 0;
    
    public CFrame(int x, int y, int w, int h)
    {
        setLocation(x, y);
        setSize(w, h);
        _graphics2D = new Graphics2D(x, y, w, h);

    }
    
    public void setVisible(boolean value)
    {
        _visible = value;
        if(_visible)
        {
            _graphics2D.clear();
            paint(_graphics2D);
        }
        else
        {
            _graphics2D.clear();
        }
    }
    
}

package cawt.event;

import cawt.CAWTEvent;
import cawt.Component;

public class ComponentEvent extends CAWTEvent
{

    public ComponentEvent(Component source, int id)
    {
        super(source, id);
    }
    
    public Component getComponent()
    {
        return (Component)getSource();
    }

}

package cawt.event;

import cawt.CAWTEvent;
import cawt.Component;

public class KeyEvent extends ComponentEvent
{
    public static int KEY_RELEASED = 1;
    protected int _keyCode = 0;
    
    public KeyEvent(Component source, int type, int keyCode)
    {
        super(source, type);
        _keyCode = keyCode;
    }
    
    public int getKeyCode()
    {
        return _keyCode;
    }
    
    //Override
    public Component getSource()
    {
        return (Component)super.getSource();
    }
}

package cawt;

public class CAWTEvent
{
    Object _source = null;
    int _id = 0;
    
    public CAWTEvent(Object source, int id)
    {
        _source = source;
        _id = id;
    }
    
    public Object getSource()
    {
        return _source;
    }
}

package cawt;

import cawt.event.KeyEvent;
import java.io.IOException;

import jline.console.ConsoleReader;

public class CEventManager
{
    CFrame _frame = null;
    ConsoleReader _console = null;
    public CEventManager(CFrame frame) throws IOException
    {
        _frame = frame;
        _console = new ConsoleReader();
    }
    
    public void start() throws IOException
    {
        while(true)
        {
            
            keyEvent();
          
        }
    }
    
    private void keyEvent() throws IOException
    {
        while(true)
        {
            int key = _console.readCharacter(true);
            KeyEvent e = new KeyEvent(_frame, KeyEvent.KEY_RELEASED, key);
        }
    }
    
    public static void main(String[] argv)
    {
        CEventManager m;
        try
        {
            m = new CEventManager(new CFrame(0, 0, 100, 50));
            m.start();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}

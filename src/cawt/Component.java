package cawt;

import java.awt.Dimension;
import java.awt.Point;

import CLIGraphics.CRectangle;
import CLIGraphics.Graphics2D;
import CLIGraphics.Shape;

public class Component
{
    protected Dimension _size = new Dimension(10, 10);
    protected Point _loc = new Point(0, 0);
    protected Graphics2D _graphics2d = null;
    
    public void setSize(Dimension size)
    {
        _size.width = size.width;
        _size.height = size.height;
    }
    
    public void setSize(int w, int h)
    {
        _size.width = w;
        _size.height = h;
    }
    
    public void setLocation(int x, int y)
    {
        _loc.x = x;
        _loc.y = y;
    }
    
    public void setLocation(Point p)
    {
        _loc.x = p.x;
        _loc.y = p.y;
    }
    
    public Point getLocation()
    {
        return new Point(_loc);
    }
    
    public int getX()
    {
        return _loc.x;
    }
    
    public int getY()
    {
        return _loc.y;
    }
    
    public Dimension getSize()
    {
        return new Dimension(_size);
    }
    
    public int getWidth()
    {
        return _size.width;
    }
    
    public int getHeight()
    {
        return _size.height;
    }
    
    public void repaint()
    {
        if(_graphics2d != null)
        {
            paint(_graphics2d);
        }
    }
    
    public void paint(Graphics2D g)
    {
        _graphics2d = g;
    }
    
    public Shape getClip()
    {
        return new CRectangle(_loc.x, _loc.y, _size.width, _size.height);
    }
    
    protected void proecessEvent(CAWTEvent e)
    {
        
    }
    
    public void dispatchEvent(CAWTEvent e)
    {
        proecessEvent(e);
    }
}

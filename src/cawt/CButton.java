package cawt;

import CLIGraphics.CRectangle;
import CLIGraphics.Graphics2D;

public class CButton extends CComponent
{
    String _text = "";
    CRectangle _rec = null;
    
    public CButton(String text)
    {
        _text = text;
        setSize(text.length() + 4, 3);
        _rec = new CRectangle(0, 0, getWidth(), getHeight());
    }
    
    public void paintComponent(Graphics2D g)
    {
       g.draw(_rec);
       g.drawString(_text, 1, 1);
    }
}

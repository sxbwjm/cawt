package cawt;

import static org.fusesource.jansi.Ansi.ansi;

import java.awt.Color;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.fusesource.jansi.AnsiConsole;

import flow.FlowData;
import flow.FlowObject;
import flow.FlowPanel;
import CLIGraphics.Graphics2D;
import CLIGraphics.MyAnsi;

public class CTable extends CComponent
{
    protected int[] _colWidth = null;
    protected int[] _customColWidth = null;
    protected TableModel      _model    = null;

    public CTable()
    {
        setModel(new DefaultTableModel());
    }
    
    public CTable(TableModel model)
    {
        setModel(model);
    }

    public void setModel(TableModel model)
    {
        _model = model;
    }

    private int[] getDefaultColWidth()
    {
        int colNum = _model.getColumnCount();
        int[] width = new int[colNum];
        if(colNum == 0)
        {
            return width;
        }
        int defaultWidth = getWidth() / colNum;
        for (int i = 0; i < colNum; i++)
        {
            width[i] = defaultWidth;
        }
        
        return width;
    }
    
    public void setColWidth(int[] colWidth)
    {
        _customColWidth = new int[_model.getColumnCount()];
        for(int i=0; i<_customColWidth.length && i<colWidth.length; i++)
        {
            _customColWidth[i] = colWidth[i];
        }
    }

    @Override
    public void paintComponent(Graphics2D g)
    {
        MyAnsi ansi = g.getAnsi();
        
        if(_customColWidth == null)
        {
            _colWidth = getDefaultColWidth();
        }
        else
        {
            _colWidth = _customColWidth;
        }
       // System.out.println("table size:" + getWidth() + " " + getHeight());
        // paint header
        if(_model.getColumnCount() > 0)
        {
            printHeader(ansi);
        }
        if(_model.getRowCount() > 0)
        {
            printRow(ansi);
        }
        
        System.out.print(ansi.reset());
        System.out.flush();
    }

    private void printHeader(MyAnsi ansi)
    {
        int r = 1;
        int c = 1;
        // for every actual column
        int colCount = _model.getColumnCount();
        printHLine(ansi, 1);
        for (int col = 0; col < colCount; col++)
        {
            ansi.appendString("|", r + 1, c);
            // draw header name
            ansi.appendString(_model.getColumnName(col), r + 1, c + 1);

            c += _colWidth[col];
        }
        ansi.appendString("|", r + 1, c);

        printHLine(ansi, r + 2);
    }

    private void printHLine(MyAnsi ansi, int r)
    {
        int c = 1;
        for (int col = 0; col < _colWidth.length; col++)
        {
            for(int i=0; i<_colWidth[col]; i++)
            {
                ansi.appendString("-", r, c);
                c++;
            }
        }
        
    }

    private void printRow(MyAnsi ansi)
    {
        int r = 4;
        int c = 1;
        // for every actual column
        int colCount = _model.getColumnCount();

        for (int row = 0; row < _model.getRowCount(); row++)
        {
            c = 1;
            for (int col = 0; col < colCount; col++)
            {
                ansi.appendString("|", r, c);
                // draw header name
                String value = (String) _model.getValueAt(row, col);
                ansi.appendString(value, r, c + 1);
                c += _colWidth[col];
            }
            ansi.appendString("|", r, c);

            printHLine(ansi, r + 1);
            r += 2;
        }

    }

    static public void main(String[] argv)
    {
        CFrame cframe = new CFrame(0, 0, 100, 100);

        String[] h = { "header1", "header2", "header3" };
        String[][] data = { { "col11", "col12", "col13" },
                { "col21", "col22", "col23" } };
        CTable t = new CTable(new DefaultTableModel(data, h));
        t.setSize(100, 50);
        cframe.add(t);
        cframe.setVisible(true);
        // pane.repaint();

        System.out.print(ansi().cursor(30, 1));
        AnsiConsole.systemUninstall();
    }

    public int getRowCount()
    {
        // TODO Auto-generated method stub
        return _model.getRowCount();
    }

    public int getColumnCount()
    {
        return _model.getColumnCount();
    }

    public String getValueAt(int r, int c)
    {
        // TODO Auto-generated method stub
        return (String)_model.getValueAt(r, c);
    }
}
